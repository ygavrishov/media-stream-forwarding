import Peer, { CallOption } from "peerjs";
import { SdpCodecs, SdpTransformer } from "../lib/sdp-transformer";

export class StreamForwarder {
    public peerId?: string;
    public readonly peer: Peer;

    public open?: (peerId: string) => void;
    public streamReceived?: (stream: MediaStream) => void;

    constructor() {
        this.peer = new Peer(undefined, {
            debug: 3,
            host: 'vs-signaling-server-dev01.azurewebsites.net',
            port: 443,
            secure: true,
        });
        console.log('creating...');

        this.peer.on('open', (id) => {
            this.peerId = id;
            console.log('Receiver Peer Id   : ' + this.peerId);
            this.open?.(id);
        });

        this.peer.on('call', mediaConnection => {
            console.log('on call', mediaConnection);
            mediaConnection.answer(undefined);
            mediaConnection.on('stream', mediaStream => {
                console.log('on stream', mediaStream);
                this.streamReceived?.(mediaStream);
            });
        });
    }

    public async start(receiverId: string | undefined, stream: MediaStream) {
        console.log('start..');
        if (!receiverId) return;
        this.peer.call(receiverId, stream, {
            sdpTransform: (sdp: string) => {
                return new SdpTransformer(sdp).ChangeCodec(SdpCodecs.H264).ToSdpString()
            }
        } as CallOption);
    }
}
