import { MediaConnection } from "lib/media-connection";
import Peer, { AnswerOption } from "peerjs";
import { SdpCodecs, SdpTransformer } from "../lib/sdp-transformer";

export class StreamReceiver {
    public peerId?: string;
    public readonly peer: Peer;

    public open?: (peerId: string) => void;
    public streamReceived?: (connection: MediaConnection, stream: MediaStream) => void;

    constructor() {
        this.peer = new Peer(undefined, {
            debug: 3,
            host: 'vs-signaling-server-dev01.azurewebsites.net',
            port: 443,
            secure: true,
        });
        console.log('creating...');

        this.peer.on('open', (id) => {
            this.peerId = id;
            console.log('Receiver Peer Id   : ' + this.peerId);
            this.open?.(id);
        });

        this.peer.on('call', mediaConnection => {
            console.log('on call', mediaConnection);
            mediaConnection.answer(undefined, {
                sdpTransform: (sdp: string) => {
                    return new SdpTransformer(sdp).ChangeCodec(SdpCodecs.H264).ToSdpString()
                }
            } as AnswerOption);
            mediaConnection.on('stream', mediaStream => {
                console.log('on stream', mediaStream);
                this.streamReceived?.(mediaConnection as MediaConnection, mediaStream);
            });
        });
    }
}
