import Peer from "peerjs";

export class StreamSender {
    public peerId?: string;
    public readonly peer: Peer;

    public open?: (peerId: string) => void;

    constructor() {
        this.peer = new Peer(undefined, {
            debug: 3,
            host: 'vs-signaling-server-dev01.azurewebsites.net',
            port: 443,
            secure: true,
        });
        console.log('creating...');

        this.peer.on('open', (id) => {
            this.peerId = id;
            console.log('Sender Peer Id: ' + this.peerId);
            this.open?.(id);
        });
    }

    public async start(receiverId?: string) {
        console.log('start..');
        if (!receiverId) return;
        const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
        this.peer.call(receiverId, stream);
    }
}