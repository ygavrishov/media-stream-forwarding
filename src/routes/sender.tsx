import { StreamSender } from 'logic/stream-sender';
import React, { useMemo, useState } from 'react'
interface Props {
}


export const Sender: React.FC<Props> = () => {
  const [senderPeerId, setSenderPeerId] = useState<string>();
  const [receiverPeerId, setReceiverPeerId] = useState<string>();
  const logic = useMemo(() => {
    const sender = new StreamSender();
    sender.open = (peerId) => setSenderPeerId(peerId);
    return sender;
  }, []);

  return (
    <>
      <h1>
        Sender, {senderPeerId}
      </h1>
      <div>
        <span>send to {receiverPeerId}</span><br />
        <input type='text' value={receiverPeerId} onChange={event => setReceiverPeerId(event.target.value)} /> <br />
        <button onClick={() => logic.start(receiverPeerId)} >Start</button>
      </div>
    </>
  );
};
