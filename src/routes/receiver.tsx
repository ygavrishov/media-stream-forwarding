import { StreamReceiver } from 'logic/stream-receiver';
import React, { useMemo, useRef, useState } from 'react'

interface Props {
}

export const Receiver: React.FC<Props> = () => {
  const video = useRef<HTMLVideoElement>(null);
  const [receiverPeerId, setReceiverPeerId] = useState<string>();
  const logic = useMemo(() => {
    const sender = new StreamReceiver();
    sender.open = (peerId) => setReceiverPeerId(peerId);
    sender.streamReceived = (connection, stream) => {
      console.log('stream received:', connection, stream);
      // const presenter = new MediaPresenter(connection);

      if (video.current) {
        // presenter.ShowRemoteOn(video.current);
        video.current.srcObject = stream;
        video.current.load();
      }
    }
    return sender;
  }, []);
  console.log(logic);

  return (
    <>
      <h1>
        Receiver, {receiverPeerId}
      </h1>
      <div>
        <video ref={video} autoPlay />
      </div>
    </>
  );
};
