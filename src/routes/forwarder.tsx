import { StreamForwarder } from 'logic/stream-forwarder';
import React, { useEffect, useMemo, useRef, useState } from 'react'

interface Props {
}

export const Forwarder: React.FC<Props> = () => {
  const video = useRef<HTMLVideoElement>(null);
  const [forwarderPeerId, setForwarderPeerId] = useState<string>();
  const [receiverPeerId, setReceiverPeerId] = useState<string>();

  const logic = useMemo(() => {
    const sender = new StreamForwarder();
    return sender;
  }, []);

  useEffect(() => {
    logic.open = peerId => setForwarderPeerId(peerId);
  }, [logic])

  useEffect(() => {
    logic.streamReceived = stream => {
      console.log('stream received:', stream);

      if (video.current) {
        video.current.srcObject = stream;
        video.current.load();
      }
      logic.start(receiverPeerId, stream);
    }
  }, [logic, receiverPeerId]);

  return (
    <>
      <h1>
        Forwarder, {forwarderPeerId}
      </h1>
      <div>
        <span>Forward to {receiverPeerId}</span><br />
        <input type='text' value={receiverPeerId} onChange={event => setReceiverPeerId(event.target.value)} /> <br />
        <video ref={video} autoPlay />
      </div>
    </>
  );
};
