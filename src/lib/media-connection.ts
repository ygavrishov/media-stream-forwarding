import Peer from "peerjs";

export interface MediaConnection extends Peer.MediaConnection {
    readonly localStream: MediaStream;
    readonly remoteStream: MediaStream;
}
