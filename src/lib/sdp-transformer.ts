import sdpTransform, { SessionDescription } from "sdp-transform";

export enum SdpCodecs {
    H264 = "h264",
    VP8 = "vp8",
    VP9 = "vp9",
}

export class SdpTransformer {

    private sdp: SessionDescription

    public constructor(sdp: string) {
        this.sdp = sdpTransform.parse(sdp);
    }

    public ChangeCodec(codec: SdpCodecs) {

        for (let i = 0; i < this.sdp.media.length; i++) {
            let media = this.sdp.media[i];
            if (media.type == "video") {
                let rtmpNumber = 0;
                for (let j = 0; j < media.rtp.length; j++) {
                    let rtp = media.rtp[j];
                    if (rtp.codec.toLowerCase() == codec.toLowerCase()) {
                        rtmpNumber = rtp.payload;
                        break;
                    }
                }

                if (media.payloads !== undefined) {
                    let payloads = sdpTransform.parsePayloads(media.payloads);
                    payloads = payloads.filter(e => e != rtmpNumber);
                    payloads = [rtmpNumber, ... payloads];
                    media.payloads = payloads.join(" ");
                }
                
            }
        }
        
        return this;
    }

    public ToSdpString() {
        return sdpTransform.write(this.sdp);
    }
}