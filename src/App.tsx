import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import { Sender } from 'routes/sender';
import { Forwarder } from 'routes/forwarder';
import { Receiver } from 'routes/receiver';

function App() {
  return (
    <div className="App">
      <Router>
        <Route path='/sender' component={Sender} />
        <Route path='/forwarder' component={Forwarder} />
        <Route path='/receiver' component={Receiver} />
      </Router>
    </div>
  );
}

export default App;
